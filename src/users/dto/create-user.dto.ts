import {
  ArrayNotEmpty,
  IsArray,
  IsEmail,
  IsNotEmpty,
  Length,
} from 'class-validator';
export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(8, 32)
  password: string;

  @IsNotEmpty()
  @Length(8, 32)
  fullName: string;

  @IsArray()
  @ArrayNotEmpty()
  roles: ('admin' | 'user')[];
  gender: 'male' | 'female' | 'others';
}
